#!/usr/bin/env python3

"""Module takes a user input of an email addressa and returns a boolean
identifying if email is compliant to a standard set of conditions
"""

import string


def get_email_addresses(file):
    """ Loads emails from the specified text file"""
    with open(file, 'r') as list_of_emails:
        emails = list(list_of_emails.read().splitlines())
    return emails


def parts(emailaddr):
    """ Identifies the location of the last @ symbol in the address"""
    at_position = False
    for i, element in enumerate(emailaddr):
        if element == '@':
            at_position = i
    if at_position:
        return emailaddr[:at_position], emailaddr[at_position + 1:]
    else:
        return False, "No `@`"


def check_local(part):
    """ Tests compliance of local part of email: no whitespace or quotes."""
    excluded = "\"'()" + string.whitespace
    for char in part:
        if char not in string.printable or char in excluded:
            return False
    return True


def check_domain(part):
    """ Tests compliance of domain part of email"""
    for char in part:
        if char not in string.ascii_lowercase:
            if char != '.':
                return False
    if '..' in part:
        return False
    if '.' not in part:
        return False
    if (part[-1] not in string.ascii_lowercase
            or part[-2] not in string.ascii_lowercase):
        return False
    return True


def check_email(email):
    """ Call for checks, False present in email if no '@' symbol"""
    if False in email:
        return False, email[0]
    if check_local(email[1]) is False:
        return False, email[0]
    if check_domain(email[2]) is False:
        return False, email[0]
    else:
        return True, email[0]


def email_list():
    """Accepts user input of an email address to test for compliance:

    Type an email address, or leave blank and hit <enter> to run the test
    using a list of emails in the file 'testemails' in the same folder
    """
    choice = str(input("Enter email: "))
    if not choice:
        test_email_filename = 'testemails'
        list_of_emails = get_email_addresses(test_email_filename)
    else:
        list_of_emails = [choice,]

    for index, element in enumerate(list_of_emails):
        email_parts = parts(element)
        list_of_emails[index] = (element,
                                 email_parts[0],
                                 email_parts[1])
    return list_of_emails


def main():
    """Calls check_email() with a list of email address, local part and domain"""
    for email in email_list():
        result, emailaddr = check_email(email)
        if result:
            print('Good: ' + emailaddr)
        else:
            print('Bad:  ' + emailaddr)


if __name__ == "__main__":
    main()
